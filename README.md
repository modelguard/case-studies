# Case Studies

Case studies that have been done with ModelGuard. To run the case studies,
proceed to the docker folder and run `./build_docker.sh`, followed by 
`./run_docker.sh`. This will drop into a shell in a ModelGuard-capable 
container. To run a case study, proceed to the desired case study under the 
`./case_studies` directory.

The `config_builder.py` script for each case study should not be necessary, as 
configuration files have already been provided.
The `run_experiments.py` script will evaluate all the configurations for a 
particular case study. Note that multiple trials will be run, potentially requiring
a significant amount of time. The number of trials can be modified by changing the value in the script.
The `./generate_figures.py` script will evaluate the results and generate figures
similar to what appears in the ADHS'21 ModelGuard paper.