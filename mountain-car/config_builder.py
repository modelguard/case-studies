#!/usr/bin/python3

import math
import random
import sys

import numpy as np
import scipy.io as sio

CONFIG_DIR = "configs"

ITERATIONS = 20
MIN_NOISE = 0
MAX_NOISE = 0
SEED = 0

EPSILON = 0.005
ITERATION_LIMIT = 2500000
REPORTING_FREQUENCY = 10000

class MountainCar:
    def __init__(self):
        self.min_action = -1.0
        self.max_action = 1.0
        self.min_position = -1.2
        self.max_position = 0.6
        self.max_speed = 0.07
        self.power = 0.0015

        self.reset()

    def step(self, action):
        force = min(max(action, self.min_action), self.max_action)
        self.velocity += force * self.power - 0.0025 * math.cos(3 * self.position)
        if (self.velocity > self.max_speed): self.velocity = self.max_speed
        if (self.velocity < -self.max_speed): self.velocity = -self.max_speed
        self.position += self.velocity
        if (self.position > self.max_position): self.position = self.max_position
        if (self.position < self.min_position): self.position = self.min_position
        if (self.position == self.min_position and self.velocity < 0): self.velocity = 0

    def reset(self):
        self.position = random.uniform(self.min_position, self.max_position)
        self.velocity = 0.0

def main():
    np.random.seed(SEED)

    mc = MountainCar()

    for i in range(ITERATIONS):
        last_position = 0
        cur_position = 0
        action = 0

        mc.reset()
        last_position = np.clip(mc.position + random.uniform(MIN_NOISE, MAX_NOISE), -1.2, 0.6)

        action = random.uniform(-1.0, 1.0)
        mc.step(action)

        cur_position = np.clip(mc.position + random.uniform(-MAX_NOISE, -MIN_NOISE), -1.2, 0.6)

        config = '''
model:
  type: nn
  filename: model.tflite
  state-space:
    - [-1.2, 0.6]
    - [-0.07, 0.07]
  input-space:
    - [-1.0, 1.0]
  output-space:
    - [-1.2, 0.6]
    - [-1.2, 0.6]
  lipschitz: 3.4767
  batch: 1000

input-trace: [''' + str(action) + ''']
output-trace: [''' + str(last_position) + ',' + str(cur_position) + '''] 

epsilon: ''' + str(EPSILON) + '''
iteration-limit: ''' + str(ITERATION_LIMIT) + '''
progress-frequency: ''' + str(REPORTING_FREQUENCY) + '''
progress-filename: $FILENAME$

# ModelGuard Options
numBins: 1e5
deltas: [0.25, 0.1, 0.05]
'''
        with open(CONFIG_DIR + '/mountain_car_'+str(MIN_NOISE)+'_'+str(MAX_NOISE)+'_'+str(i)+'.yml', 'w') as f:
          f.write(config)



if __name__ == '__main__':
  main()