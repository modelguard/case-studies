#!/usr/bin/python3

import re
import os
import subprocess
import time
import shutil
from concurrent.futures import ThreadPoolExecutor

CONFIG_DIR = 'configs'
MODELGUARD_RESULTS_DIR = 'results'
MODELGUARD_EXECUTABLE = '/modelguard/build/apps/run_modelguard'
NUM_TRIALS = 5

def run_modelguard(base_filename, filename, output_filename):
    print('EVALUATING MODELGUARD ON ' + base_filename)
    with open(filename.path) as f:
        config = f.read()
        modified_config = re.sub(r'\$FILENAME\$', output_filename, config)

    completed = subprocess.run(MODELGUARD_EXECUTABLE, input=modified_config, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, shell=True)
    if completed.returncode < 0:
        print(completed.stderr)
    else:
        print(completed.stdout)

print('REMOVE EXISTING MODELGUARD RESULTS')
shutil.rmtree(MODELGUARD_RESULTS_DIR)
os.mkdir(MODELGUARD_RESULTS_DIR)

start_time = time.time()

# Run ModelGuard experiments
for i in range(NUM_TRIALS):
    TRIAL_RESULTS_DIR = MODELGUARD_RESULTS_DIR + '/TRIAL_' + str(i)
    print('''
===========================================================
    TRIAL ''' + str(i) + '''
===========================================================
''')
    try:
        os.mkdir(TRIAL_RESULTS_DIR)
    except OSError as error:
        # Already exists, ignore
        pass

    with ThreadPoolExecutor(max_workers=os.cpu_count() - 1) as pool:
        for filename in os.scandir(CONFIG_DIR):
            if filename.path.endswith('.yml'):
                base_filename = filename.path.strip('.yml')[len(CONFIG_DIR) + 1:]
                output_filename = TRIAL_RESULTS_DIR + '/' + base_filename + '.txt'

                pool.submit(run_modelguard, base_filename, filename, output_filename)

elapsed_time = time.time() - start_time

with open('total_time.txt', 'w') as f:
    f.write(str(elapsed_time))
