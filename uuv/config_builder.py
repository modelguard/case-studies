#!/usr/bin/python3

import os
import numpy as np

CONFIG_DIR = 'configs'
TRACE_DIR = 'traces'

EPSILON = 0.19
ITERATION_LIMIT = 20000000
REPORTING_FREQUENCY = 500000

def main():
  for filename in os.scandir(TRACE_DIR):
    if filename.path.endswith('.npy'):
      base_filename = filename.path.strip('.npy')[len(TRACE_DIR) + 1:]

      trace = np.load(filename.path)
      for i in range(45,90,5):
          input_trace = []
          output_trace = []

          input_trace.append(trace[i,2])
          input_trace.append(trace[i+1,2])
          input_trace.append(trace[i+2,2])
          input_trace.append(trace[i+3,2])
          input_trace.append(trace[i,3])
          input_trace.append(trace[i+1,3])
          input_trace.append(trace[i+2,3])
          input_trace.append(trace[i+3,3])

          output_trace.append(trace[i,0])
          output_trace.append(trace[i+1,0])
          output_trace.append(trace[i+2,0])
          output_trace.append(trace[i+3,0])
          output_trace.append(trace[i+4,0])
          output_trace.append(trace[i,1])
          output_trace.append(trace[i+1,1])
          output_trace.append(trace[i+2,1])
          output_trace.append(trace[i+3,1])
          output_trace.append(trace[i+4,1])

          config = '''
model:
  type: nn
  filename: model.tflite
  state-space:
    - [-0.16874763, 0.17047971]
    - [-0.0177354, 0.0175227]
    - [-0.0384323, -0.00697203]
    - [-0.05306379, 0.05419577]
  input-space:
    - [-3.141592653589793, 3.141592653589793]
    - [-3.141592653589793, 3.141592653589793]
    - [-3.141592653589793, 3.141592653589793]
    - [-3.141592653589793, 3.141592653589793]
    - [0.514, 2.5]
    - [0.514, 2.5]
    - [0.514, 2.5]
    - [0.514, 2.5]
  output-space:
    - [-3.141592653589793, 3.141592653589793]
    - [-3.141592653589793, 3.141592653589793]
    - [-3.141592653589793, 3.141592653589793]
    - [-3.141592653589793, 3.141592653589793]
    - [-3.141592653589793, 3.141592653589793]
    - [0.514, 2.5]
    - [0.514, 2.5]
    - [0.514, 2.5]
    - [0.514, 2.5]
    - [0.514, 2.5]
  lipschitz: 64 
  batch: 1000
  mse: 0.18627676367759705

input-trace: [''' + str(input_trace[0]) + ', ' + str(input_trace[1]) + ', ' + str(input_trace[2]) + ', ' + str(input_trace[3]) + ', ' + str(input_trace[4]) + ', ' + str(input_trace[5]) + ', ' + str(input_trace[6]) + ', ' + str(input_trace[7]) + ''']
output-trace: [''' + str(output_trace[0]) + ', ' + str(output_trace[1]) + ', ' + str(output_trace[2]) + ', ' + str(output_trace[3]) + ', ' + str(output_trace[4]) + ', ' + str(output_trace[5]) + ', ' + str(output_trace[6]) + ', ' + str(output_trace[7]) + ', ' + str(output_trace[8]) + ', ' + str(output_trace[9]) + '''] 

epsilon: ''' + str(EPSILON) + '''
iteration-limit: ''' + str(ITERATION_LIMIT) + '''
progress-frequency: ''' + str(REPORTING_FREQUENCY) + '''
progress-filename: $FILENAME$

# ModelGuard Options
numBins: 1e9
deltas: [0.25, 0.1, 0.05]
'''
          with open(CONFIG_DIR + '/' + base_filename + '_' + str(i) +'.yml', 'w') as f:
            f.write(config)

if __name__=='__main__':
  main()