#!/usr/bin/python3

import os
import matplotlib.pyplot as plt
import numpy as np

MODELGUARD_RESULTS_DIR = 'results'
FIGURE_DIR = 'figures'

def is_consistent(results_contents):
    return results_contents[-1].split(',')[2].strip() == '1'

def parse_result(contents):
    result = contents[-1].split(',')[:-1]

    iterations = int(result[1].strip())
    elapsed_time = float(result[0].strip())
    confidences = [float(entry.strip()) for entry in result[2:]]

    r = [elapsed_time, iterations]
    r.extend(confidences)
    return r

def process_file(results_file):
    with open(results_file, 'r') as f:
        contents = f.readlines()[1:]
        consistent = is_consistent(contents)

        if consistent:
            footer = parse_result(contents)
            return True, np.array(footer)
        else:
            parsed_contents = []
            for line in contents:
                sp = line.split(',')[:-1]

                iterations = int(sp[1].strip())
                elapsed_time = float(sp[0].strip())
                confidences = [float(entry.strip()) for entry in sp[2:]]
                
                parsed_line = [elapsed_time, iterations]
                parsed_line.extend(confidences)
                parsed_contents.append(parsed_line)
            return False, np.array(parsed_contents)

def process_trial(trial_dir):
    consistent_iterations = []

    elapsed_time = None
    iterations = None
    delta_25 = None
    delta_1 = None
    delta_05 = None

    for filename in os.scandir(trial_dir):
        consistent, arr = process_file(filename)
        
        if consistent:
            consistent_iterations.append(arr[1])
        else:
            if elapsed_time is None:
                elapsed_time = arr[:,0]
                iterations = arr[:,1]
                delta_25 = arr[:,2]
                delta_1 = arr[:,3]
                delta_05 = arr[:,4]
            else:
                elapsed_time = np.vstack((elapsed_time, arr[:,0]))
                delta_25 = np.vstack((delta_25, arr[:,2]))
                delta_1 = np.vstack((delta_1, arr[:,3]))
                delta_05 = np.vstack((delta_05, arr[:,4]))
    
    return np.array(consistent_iterations), elapsed_time, iterations, delta_25, delta_1, delta_05

def process(fig, axs):
    consistent_iterations = None
    elapsed_time = None
    iterations = None
    delta_25 = None
    delta_1 = None
    delta_05 = None

    for entry in os.scandir(MODELGUARD_RESULTS_DIR):
        if entry.is_dir():
            ci, et, i, d25, d1, d05 = process_trial(entry)

            if consistent_iterations is None:
                consistent_iterations = ci
                elapsed_time = et
                iterations = i
                delta_25 = d25
                delta_1 = d1
                delta_05 = d05
            else:
                consistent_iterations = np.vstack((consistent_iterations, ci))
                elapsed_time = np.vstack((elapsed_time, et))
                delta_25 = np.vstack((delta_25, d25))
                delta_1 = np.vstack((delta_1, d1))
                delta_05 = np.vstack((delta_05, d05))


    # Plot information from 'inconsistent' traces
    m25 = np.mean(delta_25, 0)
    s25 = np.std(delta_25, 0)

    m1 = np.mean(delta_1, 0)
    s1 = np.std(delta_1, 0)

    m05 = np.mean(delta_05, 0)
    s05 = np.std(delta_05, 0)
    
    axs[0].plot(iterations, m25, linewidth=1.5, color='#0072BD', label='MG-0.25')
    axs[0].fill_between(iterations, m25+s25, m25-s25, alpha=0.3, color=(0, 0.4470, 0.7410), linewidth=0)

    axs[0].plot(iterations, m1, linewidth=1.5, linestyle='--', color='#EDB120', label='MG-0.1')
    axs[0].fill_between(iterations, m1+s1, m1-s1, alpha=0.3, color=(0.9290, 0.6940, 0.1250), linewidth=0)

    axs[0].plot(iterations, m05, linewidth=1.5, linestyle=':', color='#77AC30', label='MG-0.05')
    axs[0].fill_between(iterations, m05+s05, m05-s05, alpha=0.3, color=(0.4660, 0.6740, 0.1880), linewidth=0)


    # Plot information from 'consistent' traces
    mc = consistent_iterations
    N = np.size(mc)
    mc_sorted = np.insert(np.sort(mc), 0, 0)
    F = np.array(range(0,N+1))/float(N)

    axs[1].plot(mc_sorted, F, label='MG')

    eval_time = np.divide(elapsed_time, iterations)
    with open(FIGURE_DIR + '/table1.txt', 'a') as table_file:
        table_file.write('UUV ' + str(np.min(eval_time) * 1e6) + ' ' + str(np.mean(eval_time) * 1e6) + ' ' + str(np.max(eval_time) * 1e6) +'\n')


def main():
    fig, axs = plt.subplots(2)
    process(fig, axs)

    axs[0].set_xscale("log")
    axs[0].set_ylim([0,1.0])
    axs[0].set_yticks([0, 0.25, 0.5, 0.75, 1.0])
    axs[0].margins(x=0,y=0)
    axs[0].set_xlabel('Samples')
    axs[0].set_ylabel('Confidence')
    axs[0].legend(fontsize='small')
    axs[0].grid(which='both', linewidth=0.5, alpha=0.25)

    axs[1].set_xscale("log")
    axs[1].set_ylim([0,1.0])
    axs[1].set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
    axs[1].margins(x=0,y=0)
    axs[1].set_xlabel('Samples')
    axs[1].set_ylabel('% Traces Consistent')
    axs[1].legend(fontsize='small')
    axs[1].grid(which='both', linewidth=0.5, alpha=0.25)

    plt.tight_layout()
    plt.savefig(FIGURE_DIR + '/figure3.png', pad_inches=0)

if __name__=='__main__':
    main()