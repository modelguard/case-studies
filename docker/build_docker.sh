#!/usr/bin/bash

pushd ../modelguard
docker build -f extras/Dockerfile . -t modelguard/modelguard:v1.0
popd 

docker build -f ./Dockerfile .. -t modelguard/case-studies:latest