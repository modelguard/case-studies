#!/usr/bin/bash

docker run -it \
    --rm \
    --mount type=bind,source="$(pwd)"/..,target=/modelguard/case_studies \
    modelguard/case-studies:latest